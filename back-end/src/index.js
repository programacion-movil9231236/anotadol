// Para la conexion con la base de datos de mongo
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');

const app = express();
const port = process.env.PORT || 3000;

// Conexión a MongoDB
mongoose.connect('mongodb+srv://delriomolanojuanjose:juanjose@cluster0.abxtkac.mongodb.net/?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'Error de conexión a MongoDB:'));
db.once('open', () => {
  console.log('Conexión exitosa a MongoDB.');
});

// Middleware para parsear JSON
app.use(bodyParser.json());

// Rutas de registro y login (por implementar)
app.post('/api/register', async (req, res) => {
  // Implementa la lógica de registro aquí
});

app.post('/api/login', async (req, res) => {
  // Implementa la lógica de inicio de sesión aquí
});

app.listen(port, () => {
  console.log(`Servidor en ejecución en http://localhost:${port}`);
});
