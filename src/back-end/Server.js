import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import mongoose from 'mongoose';
const { ObjectId } = mongoose.Types; // Importar ObjectId de Mongoose

const app = express();
const port = process.env.PORT || 3000;

app.use(cors({
    origin: 'http://localhost:8100',
    methods: ['GET', 'POST', 'PUT', 'DELETE'], // Agrega los métodos que necesitas
    credentials: true, // Permite enviar cookies y encabezados de autenticación
  }));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

mongoose.connect('mongodb+srv://delriomolanojuanjose:juanjose@cluster0.abxtkac.mongodb.net/?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const User = mongoose.model('User', {
  username: String,
  password: String,
});

const noteSchema = new mongoose.Schema({
  title: String,
  content: String,
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
});
const Note = mongoose.model('Note', noteSchema);

const routineSchema = new mongoose.Schema({
  title: String,
  content: String,
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
});
const Routine = mongoose.model('Routine', routineSchema);


app.listen(port, () => {
  console.log(`Servidor escuchando en el puerto ${port}`);
});

// Ruta para el registro
app.post('/api/register', (req, res) => {
  const { username, password } = req.body;
  const newUser = new User({ username, password });

  newUser.save()
  .then(() => {
    // res.status(200).json({ message: 'Usuario registrado con éxito' });
    res.status(200).json({ userId: newUser._id, message: 'Usuario registrado con éxito' });

  })
  .catch((err) => {
    res.status(500).json({ error: 'Error al registrar el usuario' });
  });
  // const token = jwt.sign({ userId: user._id }, 'tu_secreto_secreto');
});

// Ruta para el inicio de sesión
app.post('/api/login', (req, res) => {
  const { username, password } = req.body;

  User.findOne({ username, password })
    .then((user) => {
      if (!user) {
        res.status(401).json({ error: 'Credenciales inválidas' });
      } else {
        // res.status(200).json({ message: 'Inicio de sesión exitoso' });
        res.status(200).json({ userId: user._id, message: 'Inicio de sesión exitoso' });

      }
      // const token = jwt.sign({ userId: user._id }, 'tu_secreto_secreto');
    })
    
    .catch((err) => {
      res.status(500).json({ error: 'Error en el servidor' });
    });
});


// routes/note.js

app.use(express.json());

// const Note = mongoose.model('Note', noteSchema); // Agregar el modelo de nota

// Ruta para crear una nota
app.post('/api/notes/create', async (req, res) => {
  try {
    const { title, content, userId } = req.body;
    const note = new Note({ title, content, userId });
    await note.save();
    res.status(201).json(note);
  } catch (error) {
    console.error('Error al crear la nota', error);
    res.status(500).json({ error: 'Error al crear la nota' });
  }
});

// Ruta para obtener todas las notas de un usuario
app.get('/api/notes/user/:userId', async (req, res) => {
  const { userId } = req.params;
  const notes = await Note.find({ userId });
  res.json(notes);
});

// Ruta para obtener una nota por su ID
app.get('/api/notes/:id', async (req, res) => {
  const { id } = req.params;
  const note = await Note.findById(id);
  if (!note) {
    return res.status(404).json({ error: 'Nota no encontrada' });
  }
  res.json(note);
});

// Ruta para editar una nota
app.put('/api/notesedit/:id', async (req, res) => {
  const { id } = req.params;
  const { title, content, userId } = req.body;
  const note = await Note.findById(id);
  if (!note) {
    return res.status(404).json({ error: 'Nota no encontrada' });
  }
  if (!note.userId.equals(userId)) { // Comparar userId como ObjectId
    return res.status(403).json({ error: 'No tienes permiso para editar esta nota' });
  }
  note.title = title;
  note.content = content;
  await note.save();
  res.json(note);
});

// Ruta para eliminar una nota
app.delete('/api/notesdelete/:id', async (req, res) => {
  const { id } = req.params;
  const userId = req.body.userId; // Obtén userId de la solicitud
  try {
    const note = await Note.findById(id);
    if (!note) {
      return res.status(404).json({ error: 'Nota no encontrada' });
    }
    if (!note.userId.equals(userId)) { // Comparar userId como ObjectId
      return res.status(403).json({ error: 'No tienes permiso para eliminar esta nota' });
    }
    await Note.deleteOne({ _id: id }); // Utiliza deleteOne para eliminar la nota
    res.status(204).end();
  } catch (error) {
    console.error('Error al eliminar nota', error);
    res.status(500).json({ error: 'Error al eliminar nota' });
  }
});

app.post('/api/checkUsername', async (req, res) => {
  const { username } = req.body;
  const existingUser = await User.findOne({ username });

  if (existingUser) {
    res.status(400).json({ error: 'Nombre de usuario en uso' });
  } else {
    res.status(200).json({ message: 'Nombre de usuario disponible' });
  }
});







// Ruta para actualizar el nombre de usuario
// Ruta para actualizar el nombre de usuario
app.put('/api/user/updateUsername/:userId', async (req, res) => {
  const { userId } = req.params;
  const { newUsername } = req.body;

  try {
    const user = await User.findByIdAndUpdate(userId, { username: newUsername }, { new: true });

    if (!user) {
      return res.status(404).json({ error: 'Usuario no encontrado' });
    }

    res.status(200).json({ message: 'Nombre de usuario actualizado con éxito' });
  } catch (error) {
    console.error('Error al actualizar el nombre de usuario', error);
    res.status(500).json({ error: 'Error al actualizar el nombre de usuario' });
  }
});


// Ruta para actualizar la contraseña
app.put('/api/user/updatePassword/:userId', async (req, res) => {
  const { userId } = req.params;
  const { newPassword } = req.body;

  try {
    const user = await User.findByIdAndUpdate(userId, { password: newPassword }, { new: true });

    if (!user) {
      return res.status(404).json({ error: 'Usuario no encontrado' });
    }

    res.status(200).json({ message: 'Contraseña actualizada con éxito' });
  } catch (error) {
    console.error('Error al actualizar la contraseña', error);
    res.status(500).json({ error: 'Error al actualizar la contraseña' });
  }
});

// Ruta para crear una rutina
app.post('/api/routines/create', async (req, res) => {
  try {
    const { title, content, userId } = req.body;
    const routine = new Routine({ title, content, userId });
    await routine.save();
    res.status(201).json(routine);
  } catch (error) {
    console.error('Error al crear la rutina', error);
    res.status(500).json({ error: 'Error al crear la rutina' });
  }
});

// Ruta para obtener todas las rutinas de un usuario
app.get('/api/routines/user/:userId', async (req, res) => {
  const { userId } = req.params;
  const routines = await Routine.find({ userId });
  res.json(routines);
});

// Ruta para obtener una rutina por su ID
app.get('/api/routines/:id', async (req, res) => {
  const { id } = req.params;
  const routine = await Routine.findById(id);
  if (!routine) {
    return res.status(404).json({ error: 'Rutina no encontrada' });
  }
  res.json(routine);
});

// Ruta para editar una rutina
app.put('/api/routines/edit/:id', async (req, res) => {
  const { id } = req.params;
  const { title, content, userId } = req.body;
  const routine = await Routine.findById(id);
  if (!routine) {
    return res.status(404).json({ error: 'Rutina no encontrada' });
  }
  if (!routine.userId.equals(userId)) {
    return res.status(403).json({ error: 'No tienes permiso para editar esta rutina' });
  }
  routine.title = title;
  routine.content = content;
  await routine.save();
  res.json(routine);
});

// Ruta para eliminar una rutina
app.delete('/api/routines/delete/:id', async (req, res) => {
  const { id } = req.params;
  const userId = req.body.userId;
  try {
    const routine = await Routine.findById(id);
    if (!routine) {
      return res.status(404).json({ error: 'Rutina no encontrada' });
    }
    if (!routine.userId.equals(userId)) {
      return res.status(403).json({ error: 'No tienes permiso para eliminar esta rutina' });
    }
    await Routine.deleteOne({ _id: id });
    res.status(204).end();
  } catch (error) {
    console.error('Error al eliminar rutina', error);
    res.status(500).json({ error: 'Error al eliminar rutina' });
  }
});

const groupSchema = new mongoose.Schema({
  groupName: String,
  creator: String,
  password: String,
  members: [String],
  notes: [{ user: String, content: String, title : String }]
});

const Group = mongoose.model('Group', groupSchema);

// Endpoint para obtener la lista de grupos de un usuario
app.get('/getUserGroups', async (req, res) => {
  try {
    const { userId } = req.query;
    const groups = await Group.find({  $or: [{ members: userId }, { creator: userId }] });
    res.status(200).json(groups);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Endpoint para unirse a un grupo
app.post('/joinGroup', async (req, res) => {
  try {
    const { groupName, password, member } = req.body;
    const group = await Group.findOne({ groupName, password });
    if (group) {
      // Verificar si el usuario ya está en el grupo
      if (!group.members.includes(member)) {
        group.members.push(member);
        await group.save();
        res.status(200).json({ message: 'Joined group successfully' });
      } else {
        res.status(400).json({ message: 'User is already a member of this group' });
      }
    } else {
      res.status(404).json({ message: 'Group not found or password incorrect' });
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Endpoint para crear un nuevo grupo
app.post('/createGroup', async (req, res) => {
  try {
    const { groupName, creator, password } = req.body;
    const newGroup = new Group({ groupName, creator, password, members: [creator], notes: [] });
    await newGroup.save();
    res.status(201).json(newGroup);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Endpoint para salirse de un grupo
app.post('/leaveGroup', async (req, res) => {
  try {
    const { groupName, member } = req.body;
    const group = await Group.findOne({ groupName, members: member });
    if (group) {
      // Verificar si el usuario es el creador del grupo
      if (group.creator === member) {
        res.status(400).json({ message: 'El creador del grupo no puede abandonarlo. Borra el grupo en su lugar.' });
      } else {
        // Retirar al usuario del grupo
        group.members = group.members.filter(m => m !== member);
        await group.save();
        res.status(200).json({ message: 'Te has salido del grupo exitosamente.' });
      }
    } else {
      res.status(404).json({ message: 'Grupo no encontrado o no eres miembro del grupo.' });
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Endpoint para borrar un grupo
app.post('/deleteGroup', async (req, res) => {
  try {
    const { groupName, creator } = req.body;
    const group = await Group.findOne({ groupName, creator });
    if (group) {
      // Borrar el grupo
      await Group.deleteOne({ groupName, creator });
      res.status(200).json({ message: 'Grupo eliminado exitosamente.' });
    } else {
      res.status(404).json({ message: 'Grupo no encontrado o no eres el creador del grupo.' });
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Ruta para agregar una nota a un grupo
app.post('/addNoteToGroup', async (req, res) => {
  const { groupId, title, content } = req.body;

  try {
    const group = await Group.findById(groupId);

    if (!group) {
      return res.status(404).json({ error: 'Grupo no encontrado' });
    }

    // Agregar la nueva nota al grupo
    group.notes.push({ title, content }); // Asumiendo que estás utilizando la autenticación y tienes el usuario en req.user

    await group.save();

    res.status(200).json({ message: 'Nota agregada exitosamente' });
  } catch (error) {
    console.error('Error al agregar nota al grupo', error);
    res.status(500).json({ error: 'Error al agregar nota al grupo' });
  }
});

// Ruta para eliminar una nota de un grupo
app.post('/deleteNoteFromGroup', async (req, res) => {
  const { groupId, noteId } = req.body;

  try {
    const group = await Group.findById(groupId);

    if (!group) {
      return res.status(404).json({ error: 'Grupo no encontrado' });
    }

    // Encontrar y eliminar la nota del grupo
    const noteIndex = group.notes.findIndex(note => note._id.toString() === noteId);

    if (noteIndex === -1) {
      return res.status(404).json({ error: 'Nota no encontrada en el grupo' });
    }

    group.notes.splice(noteIndex, 1);

    await group.save();

    res.status(200).json({ message: 'Nota eliminada exitosamente' });
  } catch (error) {
    console.error('Error al eliminar nota del grupo', error);
    res.status(500).json({ error: 'Error al eliminar nota del grupo' });
  }
});

app.get('/getGroupNotes', async (req, res) => {
  
  const { groupId } = req.query;
  try {
  
    const group = await Group.findById(groupId);

    if (!group) {
      return res.status(404).json({ error: 'Grupo no encontrado' });
    }

    // Obtener las notas del grupo y devolverlas como respuesta
    const groupNotes = group.notes;
    res.status(200).json(groupNotes);
  } catch (error) {
    console.error('Error al obtener notas del grupo', error);
    res.status(500).json({ error: 'Error al obtener notas del grupo' });
  }
});

// Ruta para editar una nota en un grupo
app.post('/editNoteInGroup', async (req, res) => {
  const { groupId, noteId, newTitle, newContent } = req.body;

  try {
    const group = await Group.findById(groupId);

    if (!group) {
      return res.status(404).json({ error: 'Grupo no encontrado' });
    }

    // Buscar la nota en el grupo
    const noteToUpdate = group.notes.find(note => note._id.toString() === noteId);

    if (!noteToUpdate) {
      return res.status(404).json({ error: 'Nota no encontrada en el grupo' });
    }

    // Actualizar el contenido y título de la nota
    noteToUpdate.content = newContent;
    noteToUpdate.title = newTitle;

    await group.save();

    res.status(200).json({ message: 'Nota editada exitosamente' });
  } catch (error) {
    console.error('Error al editar nota del grupo', error);
    res.status(500).json({ error: 'Error al editar nota del grupo' });
  }
});
