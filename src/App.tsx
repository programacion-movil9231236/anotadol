import { IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { Redirect, Route } from 'react-router-dom';

import BlocDeNotas from './pages/BlocDeNotas';
import Calendar from './pages/Calendario';
import ConfigUsuario from './pages/ConfigUsuario';
import Grupo from './pages/Grupos';
import Home from './pages/Home';
import Inicio from './pages/Inicio';
import Login from './pages/Login';
import Menu from './pages/Menu';
import Notes from './pages/Notas';
import Register from './pages/Register';
import Rutinas from './pages/Rutinas';
import DeleteNote from './pages/deleteNote';
import EditNote from './pages/editNote';

const App: React.FC = () => (
  <IonReactRouter>
    <IonRouterOutlet>
      <Route path="/home" component={Home} exact />
      <Route path="/register" component={Register} exact />
      <Route path="/login" component={Login} exact />
      <Route path="/inicio" component={Inicio} exact />
      <Route path="/configusuario" component={ConfigUsuario} exact />
      <Route path="/blocdenotas" component={BlocDeNotas} exact />
      <Route path="/calendar" component={Calendar} exact />
      <Route path="/menu" component={Menu} exact />
      <Route path="/notes/edit/:id" component={EditNote} />
      <Route path="/notes/delete/:id" component={DeleteNote} />
      <Route path="/Notas" component={Notes} />
      <Route path="/grupo" component={Grupo} />
      <Route path="/rutinas" component={Rutinas} />
      <Redirect exact from="/" to="/home" />
    </IonRouterOutlet>
  </IonReactRouter>
);

export default App;
