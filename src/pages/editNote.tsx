// src/pages/EditNote.tsx

import {
  IonButton,
  IonContent,
  IonHeader,
  IonInput,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router';

const EditNote: React.FC<RouteComponentProps<{ id: string }>> = (props) => {
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const noteId = props.match.params.id;

  useEffect(() => {
    // Realiza una solicitud para obtener los detalles de la nota seleccionada
    Axios.get(`http://localhost:3000/api/note/${noteId}`)
      .then((response) => {
        setTitle(response.data.title);
        setContent(response.data.content);
      })
      .catch((error) => {
        console.error('Error al obtener la nota:', error);
      });
  }, [noteId]);

  const handleUpdateNote = () => {
    // Realiza una solicitud para actualizar la nota
    Axios.put(`/api/notes/${noteId}`, { title, content })
      .then(() => {
        // Redirige de vuelta a la vista de notas después de la actualización
        props.history.push('/notes');
      })
      .catch((error) => {
        console.error('Error al actualizar la nota:', error);
      });
  };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Editar Nota</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
  <IonInput
    value={title || ''}
    onIonChange={(e) => setTitle(e.detail.value || '')}
  >
    Título
  </IonInput>
  <IonInput
    value={content || ''}
    onIonChange={(e) => setContent(e.detail.value || '')}
  >
    Contenido
  </IonInput>
  <IonButton onClick={handleUpdateNote}>Guardar Cambios</IonButton>
</IonContent>

    </IonPage>
  );
};

export default EditNote;
