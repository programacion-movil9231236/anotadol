// Importación de componentes y módulos de Ionic y React
import { IonButton, IonContent, IonInput, IonLabel } from '@ionic/react';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import './Calendario'; // Importación de un módulo Calendario (no se utiliza en el código)

// Definición del componente ConfigUsuario
const ConfigUsuario: React.FC = () => {
  // Obtención de la instancia de historial de navegación de React Router
  const history = useHistory();

  // Función para navegar a la página "/menu"
  const navigatemenu = () => {
    history.push('/menu');
  };

  // Estados para el nuevo nombre de usuario, nueva contraseña y estados de edición
  const [newUsername, setNewUsername] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [isUpdatingUsername, setIsUpdatingUsername] = useState(false);
  const [isUpdatingPassword, setIsUpdatingPassword] = useState(false);

  // Obtención del ID de usuario del almacenamiento local
  const userId = localStorage.getItem('userId');

  // Función para manejar la actualización del nombre de usuario
  const handleUpdateUsername = () => {
    if (newUsername.trim() !== '') {
      fetch(`http://localhost:3000/api/user/updateUsername/${userId}`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ newUsername }),
      })
        .then((response) => {
          if (response.status === 200) {
            console.log('Nombre de usuario actualizado con éxito');
            setIsUpdatingUsername(false);
          } else {
            console.error('Error al actualizar el nombre de usuario');
          }
        });
    } else {
      console.log('El campo de nombre de usuario no puede estar en blanco.');
    }
  };

  // Función para manejar la actualización de la contraseña
  const handleUpdatePassword = () => {
    if (newPassword.trim() !== '') {
      fetch(`http://localhost:3000/api/user/updatePassword/${userId}`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ newPassword }),
      })
        .then((response) => {
          if (response.status === 200) {
            console.log('Contraseña actualizada con éxito');
            setIsUpdatingPassword(false);
          } else {
            console.error('Error al actualizar la contraseña');
          }
        });
    } else {
      console.log('El campo de contraseña no puede estar en blanco.');
    }
  };

  // Función para cancelar la edición del nombre de usuario o contraseña
  const handleCancelUpdate = (field: string) => {
    if (field === 'username') {
      setIsUpdatingUsername(false);
      setNewUsername('');
    } else if (field === 'password') {
      setIsUpdatingPassword(false);
      setNewPassword('');
    }
  };

  // Renderizado del componente
  return (
    <IonContent style={{ '--background': 'no-repeat center/cover url(/fondo.jpg)' }} className='fondo'>
      <IonButton onClick={navigatemenu}>Menu</IonButton>
      <div style={{ textAlign: 'center' }}>
        <h1>Configuración del usuario</h1>
      </div>
      <div>
        {/* Edición del nombre de usuario */}
        <IonLabel>Nombre de usuario</IonLabel>
        {isUpdatingUsername ? (
          <div>
            <IonInput
              type="text"
              value={newUsername}
              onIonChange={(e) => setNewUsername(e.detail.value!)}
            />
            <IonButton onClick={handleUpdateUsername}>Guardar</IonButton>
            <IonButton onClick={() => handleCancelUpdate('username')}>Cancelar</IonButton>
          </div>
        ) : (
          <IonButton onClick={() => setIsUpdatingUsername(true)}>Editar Nombre de Usuario</IonButton>
        )}
      </div>
      <div>
        {/* Edición de la contraseña */}
        <IonLabel>Contraseña</IonLabel>
        {isUpdatingPassword ? (
          <div>
            <IonInput
              type="password"
              value={newPassword}
              onIonChange={(e) => setNewPassword(e.detail.value!)}
            />
            <IonButton onClick={handleUpdatePassword}>Guardar</IonButton>
            <IonButton onClick={() => handleCancelUpdate('password')}>Cancelar</IonButton>
          </div>
        ) : (
          <IonButton onClick={() => setIsUpdatingPassword(true)}>Editar Contraseña</IonButton>
        )}
      </div>
    </IonContent>
  );
};

export default ConfigUsuario;
