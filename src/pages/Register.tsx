// Importa los componentes necesarios de Ionic y React
import { IonButton, IonContent, IonInput } from '@ionic/react';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

// Definición del componente Register
const Register: React.FC = () => {
  // Estados para almacenar el nombre de usuario y la contraseña
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  // Hook para la navegación
  const history = useHistory();

  // Función para manejar el registro de usuario
  const handleRegister = async () => {
    // Validar que los campos no estén en blanco
    if (!username || !password) {
      console.error('Por favor, complete todos los campos');
      return;
    }

    try {
      // Verificar si el nombre de usuario ya está en uso
      const usernameExists = await checkIfUsernameExists(username);

      if (usernameExists) {
        console.error('El nombre de usuario ya está en uso');
        return;
      }

      // Si el nombre de usuario no está en uso, procede con el registro
      const response = await fetch('http://localhost:3000/api/register', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password }),
      });

      if (response.status === 200) {
        // Registro exitoso, obtén el ID del usuario
        const data = await response.json();
        const userId = data.userId;

        // Guarda el ID del usuario en el almacenamiento local
        localStorage.setItem('userId', userId);

        // Redirige a la página del menú principal
        history.push('/menu');
      } else {
        // Error en el registro, maneja el error si es necesario
        const data = await response.json();
        console.error(data.error);
      }
    } catch (error) {
      console.error('Error al registrar:', error);
    }
  };

  // Función para verificar si el nombre de usuario ya está en uso
  const checkIfUsernameExists = async (username: string) => {
    try {
      const response = await fetch('http://localhost:3000/api/checkUsername', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username }),
      });

      if (response.status === 200) {
        // El nombre de usuario no existe, es seguro registrarlo
        return false;
      } else {
        // El nombre de usuario ya está en uso
        return true;
      }
    } catch (error) {
      console.error('Error al verificar el nombre de usuario:', error);
      return true; // Maneja cualquier error como un nombre de usuario en uso por seguridad
    }
  };

  // Función para navegar de regreso a la página de inicio
  const navigatehome = () => {
    history.push('/home'); // Cambia '/home' a la ruta a la que deseas navegar
  };

  // Renderiza el contenido del componente
  return (
    <IonContent style={{ 
      '--background': 'no-repeat center/cover url(/fondo.jpg)' 
    }} className='fondo'>
      <h1 style={{ textAlign: 'center', fontSize: '24px', margin: '20px' }}>Register</h1>
      <IonInput
        placeholder="Nombre de usuario"
        value={username}
        onIonChange={(e) => setUsername(e.detail.value!)}
      />
      <IonInput
        placeholder="Contraseña"
        type="password"
        value={password}
        onIonChange={(e) => setPassword(e.detail.value!)}
      />
      <IonButton onClick={handleRegister}>Registrarse</IonButton>
      <IonButton onClick={navigatehome}>Regresar</IonButton>
    </IonContent>
  );
};

// Exporta el componente Register
export default Register;
