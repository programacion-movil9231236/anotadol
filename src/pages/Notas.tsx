// Importa los componentes necesarios de Ionic y React
import { IonButton, IonContent, IonInput, IonItem, IonLabel, IonList } from '@ionic/react';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

// Define la interfaz para el objeto Nota
interface Nota {
  _id: string;
  title: string;
  content: string;
  userId: string;
}

// Definición del componente Notes
const Notes: React.FC = () => {
  const [notas, setNotas] = useState<Nota[]>([]); // Estado para almacenar la lista de notas
  const [notaActual, setNotaActual] = useState<Nota>({ _id: '', title: '', content: '', userId: '' }); // Estado para la nota actual
  const [viewMode, setViewMode] = useState('list'); // Estado para controlar el modo de visualización ('list', 'create', 'edit')
  const userId = localStorage.getItem('userId'); // Obtiene el ID del usuario desde el almacenamiento local
  const history = useHistory(); // Hook para la navegación

  // Función para navegar al menú principal
  const navigatemenu = () => {
    history.push('/menu');
  };

  // Efecto secundario: Cargar notas desde el servidor al iniciar la vista
  useEffect(() => {
    fetch(`http://localhost:3000/api/notes/user/${userId}`)
      .then((response) => response.json())
      .then((data) => setNotas(data))
      .catch((error) => console.error('Error al cargar notas', error));
  }, [userId]);

  // Función para crear una nueva nota
  const crearNota = (title: string, content: string) => {
    // Lógica para enviar la solicitud al servidor y manejar la respuesta
    // Actualiza el estado de las notas y cambia al modo de lista
  };

  // Función para editar una nota existente
  const editarNota = (id: string, title: string, content: string, userId: string) => {
    // Lógica para enviar la solicitud al servidor y manejar la respuesta
    // Actualiza el estado de las notas y cambia al modo de lista
  };

  // Función para eliminar una nota
  const eliminarNota = (id: string) => {
    // Lógica para enviar la solicitud al servidor y manejar la respuesta
    // Actualiza el estado de las notas
  };

  // Función para cargar una nota existente para su edición
  const cargarNotaParaEdicion = (id: string, title: string, content: string, userId: string) => {
    // Actualiza el estado de la nota actual y cambia al modo de edición
  };

  // Función para cancelar la acción actual (crear o editar nota)
  const cancelAction = () => {
    // Limpia el estado de la nota actual y cambia al modo de lista
  };

  // Renderiza el contenido del componente
  return (
    <IonContent style={{ 
      '--background': 'no-repeat center/cover url(/fondo.jpg)' 
    }} className='fondo'>
      {viewMode === 'list' && (
        // Modo de lista: Muestra la lista de notas con botones para editar y eliminar cada nota
        <IonList>
          <IonButton onClick={navigatemenu}>Menu</IonButton>
          <h1 style={{ textAlign: 'center' }}>Notas</h1>
          <div style={{ textAlign: 'center' }}>
            <IonButton onClick={() => setViewMode('create')}>Crear Nota</IonButton>
          </div>
          {notas.map((nota) => (
            <div style={{ textAlign: 'center' }}>
              <IonItem key={nota._id}>
                <IonLabel>{nota.title}</IonLabel>
                <IonButton onClick={() => cargarNotaParaEdicion(nota._id, nota.title, nota.content, nota.userId)}>
                  Editar
                </IonButton>
                <IonButton onClick={() => eliminarNota(nota._id)}>Eliminar</IonButton>
              </IonItem>
            </div>
          ))}
        </IonList>
      )}

      {(viewMode === 'create' || viewMode === 'edit') && (
        // Modo de creación o edición: Muestra campos de entrada para el título y el contenido de la nota
        <div>
          <IonInput
            value={notaActual.title}
            placeholder="Título de la nota"
            onIonChange={(e) => setNotaActual({ ...notaActual, title: e.detail.value! })}
          />
          <IonInput
            value={notaActual.content}
            placeholder="Contenido de la nota"
            onIonChange={(e) => setNotaActual({ ...notaActual, content: e.detail.value! })}
          />
          <IonButton onClick={() => (viewMode === 'create' ? crearNota(notaActual.title, notaActual.content) : editarNota(notaActual._id, notaActual.title, notaActual.content, notaActual.userId))}>Guardar</IonButton>
          <IonButton onClick={cancelAction}>Cancelar</IonButton>
        </div>
      )}
    </IonContent>
  );
};

// Exporta el componente Notes
export default Notes;
