import {
  IonButton,
  IonContent,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonNote,
} from '@ionic/react';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

// Definición del componente funcional Groups
const Groups: React.FC = () => {
  // Estados para manejar datos y estados del componente
  const [groups, setGroups] = useState([]); // Lista de grupos del usuario
  const [showForm, setShowForm] = useState(false); // Mostrar/ocultar formulario de grupos
  const [groupName, setGroupName] = useState('');
  const [groupPassword, setGroupPassword] = useState('');
  const [groupNotes, setGroupNotes] = useState([]); // Notas del grupo seleccionado
  const [selectedGroupId, setSelectedGroupId] = useState(''); // ID del grupo seleccionado
  const userId = localStorage.getItem('userId');
  const history = useHistory();
  const [newNoteTitle, setNewNoteTitle] = useState('');
  const [newNoteContent, setNewNoteContent] = useState('');
  const [showNoteForm, setShowNoteForm] = useState(false); // Mostrar/ocultar formulario de notas
  const [selectedNote, setSelectedNote] = useState(null); // Nota seleccionada
  const [viewingNoteDetails, setViewingNoteDetails] = useState(false); // Ver detalles de una nota
  const [isEditing, setIsEditing] = useState(false); // Editar nota
  const [editingNoteId, setEditingNoteId] = useState(''); // ID de la nota que se está editando
  const [editedNoteContent, setEditedNoteContent] = useState(''); // Contenido de la nota editada
  const [editedNoteTitle, setEditedNoteTitle] = useState(''); // Título de la nota editada

  // Función para navegar al menú principal
  const navigateMenu = () => {
    history.push('/menu');
  };

  // Efecto que se ejecuta al montar el componente para obtener la lista de grupos del usuario
  useEffect(() => {
    axios
      .get(`http://localhost:3000/getUserGroups?userId=${userId}`)
      .then((response) => setGroups(response.data))
      .catch((error) => console.error(error));
  }, [userId]);

  // Función para unirse a un grupo
  const joinGroup = () => {
    axios
      .post('http://localhost:3000/joinGroup', {
        groupName,
        password: groupPassword,
        member: userId,
      })
      .then((response) => {
        console.log(response.data.message);
        axios
          .get(`http://localhost:3000/getUserGroups?userId=${userId}`)
          .then((response) => setGroups(response.data))
          .catch((error) => console.error(error));
        setShowForm(false);
      })
      .catch((error) => console.error(error));
  };

  // Función para crear un nuevo grupo
  const createGroup = () => {
    axios
      .post('http://localhost:3000/createGroup', {
        groupName,
        creator: userId,
        password: groupPassword,
      })
      .then((response) => {
        console.log(response.data.message);
        axios
          .get(`http://localhost:3000/getUserGroups?userId=${userId}`)
          .then((response) => setGroups(response.data))
          .catch((error) => console.error(error));
        setShowForm(false);
      })
      .catch((error) => console.error(error));
  };

  // Función para salir de un grupo
  const leaveGroup = (groupName: any) => {
    axios
      .post('http://localhost:3000/leaveGroup', { groupName, member: userId })
      .then((response) => {
        console.log(response.data.message);
        axios
          .get(`http://localhost:3000/getUserGroups?userId=${userId}`)
          .then((response) => setGroups(response.data))
          .catch((error) => console.error(error));
      })
      .catch((error) => console.error(error));
    return false;
  };

  // Función para eliminar un grupo
  const deleteGroup = (groupName: any, creator: any) => {
    axios
      .post('http://localhost:3000/deleteGroup', { groupName, creator })
      .then((response) => {
        console.log(response.data.message);
        axios
          .get(`http://localhost:3000/getUserGroups?userId=${userId}`)
          .then((response) => setGroups(response.data))
          .catch((error) => console.error(error));
      })
      .catch((error) => console.error(error));
    return false;
  };

  // Función para ver las notas de un grupo
  const viewGroupNotes = (Id: string) => {
    axios
      .get(`http://localhost:3000/getGroupNotes?groupId=${Id}`)
      .then((response) => {
        setGroupNotes(response.data);
        setSelectedGroupId(Id);
        setShowNoteForm(false);
        setIsEditing(false);
      })
      .catch((error) => console.error(error));
  };

  // Función para agregar una nota al grupo
  const addNoteToGroup = () => {
    axios
      .post('http://localhost:3000/addNoteToGroup', {
        groupId: selectedGroupId,
        title: newNoteTitle,
        content: newNoteContent,
      })
      .then((response) => {
        console.log(response.data.message);
        axios
          .get(`http://localhost:3000/getGroupNotes?groupId=${selectedGroupId}`)
          .then((response) => setGroupNotes(response.data))
          .catch((error) => console.error(error));
        setNewNoteTitle('');
        setNewNoteContent('');
        setShowNoteForm(false);
      })
      .catch((error) => console.error(error));
  };

  // Función para mostrar/ocultar el formulario de notas
  const toggleNoteForm = () => {
    setShowNoteForm(!showNoteForm);
  };

  // Función para eliminar una nota del grupo
  const deleteNoteFromGroup = (noteId: string) => {
    axios
      .post('http://localhost:3000/deleteNoteFromGroup', {
        groupId: selectedGroupId,
        noteId,
      })
      .then((response) => {
        console.log(response.data.message);
        axios
          .get(`http://localhost:3000/getGroupNotes?groupId=${selectedGroupId}`)
          .then((response) => setGroupNotes(response.data))
          .catch((error) => console.error(error));
      })
      .catch((error) => console.error(error));
  };

  // Función para empezar a editar una nota
  const startEditingNote = (noteId: string, noteContent: string, noteTitle: string) => {
    setEditingNoteId(noteId);
    setEditedNoteContent(noteContent);
    setEditedNoteTitle(noteTitle);
    setIsEditing(true);
  };

  // Función para guardar los cambios de una nota editada
  const saveEditedNote = () => {
    axios.post('http://localhost:3000/editNoteInGroup', {
      groupId: selectedGroupId,
      noteId: editingNoteId,
      newContent: editedNoteContent,
      newTitle: editedNoteTitle,
    })
    .then((response) => {
      console.log(response.data.message);
      axios.get(`http://localhost:3000/getGroupNotes?groupId=${selectedGroupId}`)
        .then((response) => setGroupNotes(response.data))
        .catch((error) => console.error(error));
      setEditingNoteId('');
      setEditedNoteContent('');
      setEditedNoteTitle('');
      setIsEditing(false);
    })
    .catch((error) => console.error(error));
  };

  // Función para cancelar la edición de una nota
  const cancelEditingNote = () => {
    setEditingNoteId('');
    setEditedNoteContent('');
    setEditedNoteTitle('');
    setIsEditing(false);
  };

  // Función para navegar hacia atrás
  const navigateBack = () => {
    setSelectedGroupId('');
    setGroupNotes([]);
    setShowNoteForm(false);
  };

  // Renderizado del componente
  return (
    <IonContent style={{ 
      '--background': 'no-repeat center/cover url(/fondo.jpg)' 
    }} className='fondo'>
      {selectedGroupId ? (
        // Muestra las notas del grupo cuando estás dentro de un grupo
        <div>
          {!isEditing && !showNoteForm && (
            <div>
              <IonButton onClick={navigateBack}>
                Volver a la lista de grupos
              </IonButton>
              <IonButton onClick={toggleNoteForm}>
                Agregar Nueva Nota
              </IonButton>
            </div>
          )}
  
          {showNoteForm && (
            <div>
              <IonLabel>Título de la Nota</IonLabel>
              <IonInput
                value={newNoteTitle}
                onIonChange={(e) => setNewNoteTitle(e.detail.value!)}
              />
              <IonLabel>Contenido de la Nota</IonLabel>
              <IonInput
                value={newNoteContent}
                onIonChange={(e) => setNewNoteContent(e.detail.value!)}
              />
              <IonButton onClick={addNoteToGroup}>Guardar Nota</IonButton>
              <IonButton onClick={toggleNoteForm}>Cancelar</IonButton>
            </div>
          )}
  
          {!isEditing && !showNoteForm && (
            <IonList>
              {groupNotes.map((note: any) => (
                <IonItem key={note._id}>
                  <IonNote>{note.title}</IonNote>
                  <IonButton onClick={() => deleteNoteFromGroup(note._id)}>
                    Eliminar Nota
                  </IonButton>
                  <IonButton
                    onClick={() =>
                      startEditingNote(note._id, note.content, note.title)
                    }
                  >
                    Editar Nota
                  </IonButton>
                </IonItem>
              ))}
            </IonList>
          )}
  
          {isEditing && (
            <div>
              <IonLabel>Editar Título</IonLabel>
              <IonInput
                value={editedNoteTitle}
                onIonChange={(e) => setEditedNoteTitle(e.detail.value!)}
              />
              <IonLabel>Editar Contenido</IonLabel>
              <IonInput
                value={editedNoteContent}
                onIonChange={(e) => setEditedNoteContent(e.detail.value!)}
              />
              <IonButton onClick={saveEditedNote}>Guardar Cambios</IonButton>
              <IonButton onClick={cancelEditingNote}>Cancelar</IonButton>
            </div>
          )}
        </div>
        
      ) : (
        
        // Muestra la lista de grupos cuando no estás dentro de un grupo
        <div>
          {!showForm && (
            <>
              <IonButton onClick={navigateMenu}>Menu</IonButton>
              <IonButton onClick={() => setShowForm(true)}>Unirse/Crear Grupo</IonButton>
            </>
          )}
          {showForm ? (
            <div>
              <IonLabel>Nombre del Grupo</IonLabel>
              <IonInput
                value={groupName}
                onIonChange={(e) => setGroupName(e.detail.value!)}
              />
              <IonLabel>Contraseña del Grupo</IonLabel>
              <IonInput
                type="password"
                value={groupPassword}
                onIonChange={(e) => setGroupPassword(e.detail.value!)}
              />
              <IonButton onClick={joinGroup} disabled={showNoteForm || isEditing}>
                Unirse
              </IonButton>
              <IonButton onClick={createGroup} disabled={showNoteForm || isEditing}>
                Crear Grupo
              </IonButton>
              <IonButton onClick={() => setShowForm(false)}>Cancelar</IonButton>
            </div>
          ) : (
            <IonList>
              {groups.map((group: any) => (
                <IonItem
                  key={group.groupName}
                  onClick={() => viewGroupNotes(group._id)}
                >
                  <h2>{group.groupName}</h2>
                  <p>{group.creator}</p>
                  <IonButton onClick={(e) => { e.stopPropagation(); leaveGroup(group.groupName); }}>
                    Salir del Grupo
                  </IonButton>
                  {group.creator === userId && (
                    <IonButton onClick={(e) => { e.stopPropagation(); deleteGroup(group.groupName, group.creator); }}>
                      Borrar Grupo
                    </IonButton>
                  )}
                </IonItem>
              ))}
            </IonList>
          )}
        </div>
      )}
    </IonContent>
  );
};

// Exportar el componente Groups
export default Groups;
