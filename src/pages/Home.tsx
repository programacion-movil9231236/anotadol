// Importa los componentes necesarios de Ionic y React, así como el hook useHistory
import { IonButton, IonContent } from '@ionic/react';
import React from 'react';
import { useHistory } from 'react-router-dom';
import '../Style.css'; // Importa un archivo de estilos (Style.css)

// Definición del componente Home
const Home: React.FC = () => {
  const history = useHistory(); // Instancia el hook useHistory para la navegación

  // Función para manejar el clic en el botón "Iniciar Sesión"
  const handleIniciarSesionClick = () => {
    history.push('/login'); // Redirige a la ruta '/login'
  };

  // Función para manejar el clic en el botón "Registrarse"
  const handleRegistrarseClick = () => {
    history.push('/register'); // Redirige a la ruta '/register'
  };

  // Renderiza el contenido del componente
  return (
    <IonContent style={{ 
      '--background': 'no-repeat center/cover url(/fondo.jpg)' 
    }} className='fondo'>
      <h1 style={{ textAlign: 'center', fontSize: '24px', margin: '20px', color: 'black' }}>Anotador</h1>
      <IonButton onClick={handleIniciarSesionClick}>Iniciar Sesión</IonButton>
      <IonButton onClick={handleRegistrarseClick}>Registrarse</IonButton>
    </IonContent>
  );
};

// Exporta el componente Home
export default Home;
