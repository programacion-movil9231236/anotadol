// Importa los componentes necesarios de Ionic y React
import { IonButton, IonContent, IonInput, IonItem, IonLabel, IonList } from '@ionic/react';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

// Interfaz que describe la estructura de una rutina
interface Rutina {
  _id: string;
  title: string;
  content: string;
  userId: string;
}

// Definición del componente Rutinas
const Rutinas: React.FC = () => {
  // Estados para almacenar las rutinas, la rutina actual y el modo de vista (list, create, edit)
  const [rutinas, setRutinas] = useState<Rutina[]>([]);
  const [rutinaActual, setRutinaActual] = useState<Rutina>({ _id: '', title: '', content: '', userId: '' });
  const [viewMode, setViewMode] = useState('list'); // Puede ser 'list', 'create', o 'edit'
  const userId = localStorage.getItem('userId'); // Obtiene el ID del usuario actual desde el almacenamiento local

  // Hook para la navegación
  const history = useHistory();

  // Función para navegar de regreso al menú principal
  const navigatemenu = () => {
    history.push('/menu');
  };

  // Hook de efecto para cargar las rutinas desde el servidor al iniciar la vista
  useEffect(() => {
    fetch(`http://localhost:3000/api/routines/user/${userId}`)
      .then((response) => response.json())
      .then((data) => setRutinas(data))
      .catch((error) => console.error('Error al cargar rutinas', error));
  }, [userId]);

  // Función para crear una nueva rutina
  const crearRutina = (title: string, content: string) => {
    if (title.trim() !== '' && content.trim() !== '') {
      fetch('http://localhost:3000/api/routines/create', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ title, content, userId }),
      })
        .then((response) => response.json())
        .then((data) => {
          setRutinas([...rutinas, data]);
          setRutinaActual({ _id: '', title: '', content: '', userId: '' });
          setViewMode('list'); // Cambiamos a la vista de lista
        })
        .catch((error) => console.error('Error al crear rutina', error));
    }
  };

  // Función para editar una rutina existente
  const editarRutina = (id: string, title: string, content: string, userId: string) => {
    if (title.trim() !== '' && content.trim() !== '') {
      const userStringId = localStorage.getItem('userId');
      if (userStringId === userId) {
        fetch(`http://localhost:3000/api/routines/edit/${id}`, {
          method: 'PUT',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ id, title, content, userId: userStringId }),
        })
          .then((response) => response.json())
          .then((data) => {
            const rutinasActualizadas = rutinas.map((rutina) =>
              rutina._id === id ? { ...rutina, title, content } : rutina
            );
            setRutinas(rutinasActualizadas);
            setRutinaActual({ _id: '', title: '', content: '', userId: '' });
            setViewMode('list'); // Cambiamos a la vista de lista
          })
          .catch((error) => console.error('Error al editar rutina', error));
      } else {
        console.error('No tienes permiso para editar esta rutina');
      }
    }
  };

  // Función para eliminar una rutina existente
  const eliminarRutina = (id: string) => {
    fetch(`http://localhost:3000/api/routines/delete/${id}`, {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ userId }), // Enviar userId para autorización
    })
      .then((response) => {
        if (response.status === 204) {
          const rutinasFiltradas = rutinas.filter((rutina) => rutina._id !== id);
          setRutinas(rutinasFiltradas);
          setRutinaActual({ _id: '', title: '', content: '', userId: '' });
        } else if (response.status === 403) {
          console.error('No tienes permiso para eliminar esta rutina');
        } else {
          console.error('Error al eliminar rutina');
        }
      })
      .catch((error) => console.error('Error al eliminar rutina', error));
  };

  // Función para cargar una rutina existente para su edición
  const cargarRutinaParaEdicion = (id: string, title: string, content: string, userId: string) => {
    setRutinaActual({ _id: id, title, content, userId });
    setViewMode('edit'); // Cambiamos a la vista de edición
  };

  // Función para cancelar una acción de creación o edición de rutina
  const cancelAction = () => {
    setRutinaActual({ _id: '', title: '', content: '', userId: '' });
    setViewMode('list'); // Cambiamos a la vista de lista
  };

  // Renderiza el contenido del componente
  return (
    <IonContent style={{ 
      '--background': 'no-repeat center/cover url(/fondo.jpg)' 
    }} className='fondo'>
      {/* Vista de lista de rutinas */}
      {viewMode === 'list' && (
        <IonList>
          <IonButton onClick={navigatemenu}>Menu</IonButton>
          <h1 style={{ textAlign: 'center' }}>Rutinas</h1>
          <div style={{ textAlign: 'center' }}>
            <IonButton onClick={() => setViewMode('create')}>Crear Rutina</IonButton>
          </div>
          {rutinas.map((rutina) => (
            <div style={{ textAlign: 'center' }} key={rutina._id}>
              <IonItem>
                <IonLabel>{rutina.title}</IonLabel>
                <IonButton onClick={() => cargarRutinaParaEdicion(rutina._id, rutina.title, rutina.content, rutina.userId)}>
                  Editar
                </IonButton>
                <IonButton onClick={() => eliminarRutina(rutina._id)}>Eliminar</IonButton>
              </IonItem>
            </div>
          ))}
        </IonList>
      )}

      {/* Vista de creación o edición de rutina */}
      {(viewMode === 'create' || viewMode === 'edit') && (
        <div>
          <IonInput
            value={rutinaActual.title}
            placeholder="Título de la rutina"
            onIonChange={(e) => setRutinaActual({ ...rutinaActual, title: e.detail.value! })}
          />
          <IonInput
            value={rutinaActual.content}
            placeholder="Contenido de la rutina"
            onIonChange={(e) => setRutinaActual({ ...rutinaActual, content: e.detail.value! })}
          />
          <IonButton onClick={() => (viewMode === 'create' ? crearRutina(rutinaActual.title, rutinaActual.content) : editarRutina(rutinaActual._id, rutinaActual.title, rutinaActual.content, rutinaActual.userId))}>
            Guardar
          </IonButton>
          <IonButton onClick={cancelAction}>Cancelar</IonButton>
        </div>
      )}
    </IonContent>
  );
};

// Exporta el componente Rutinas
export default Rutinas;
