import { IonButton, IonContent, IonTextarea, TextareaChangeEventDetail } from '@ionic/react';
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Calendar from './Calendario';
import ConfigUsuario from './ConfigUsuario';

interface SelectedDay {
  day: number | null;
  month: number | null;
  year: number | null;
}

const BlocDeNotas: React.FC = () => {
  const history = useHistory();
  const navigatemenu = () => {
    history.push('/menu');
  };
 
  const [selectedDay, setSelectedDay] = useState<SelectedDay>({
    day: null,
    month: null,
    year: null,
  });
  
  // Load notes from LocalStorage
  const [notes, setNotes] = useState<{ [key: string]: string }>(() => {
    const savedNotes = localStorage.getItem('notes');
    return savedNotes ? JSON.parse(savedNotes) : {};
  });

  // Save notes to LocalStorage whenever they change
  useEffect(() => {
    localStorage.setItem('notes', JSON.stringify(notes));
  }, [notes]);

  const [showConfig, setShowConfig] = useState(false);

  const handleDayClick = (day: number, month: number, year: number) => {
    setSelectedDay({ day, month, year });
  };

  const handleNoteChange = (event: CustomEvent<TextareaChangeEventDetail>) => {
    setNotes({
      ...notes,
      [`${selectedDay.day}-${selectedDay.month}-${selectedDay.year}`]: event.detail.value!,
    });
  };

  const handleBackClick = () => {
    setSelectedDay({ day: null, month: null, year: null });
  };

const handleClearAll = () => {
  const { day, month, year } = selectedDay;

  if (day !== null && month !== null && year !== null) {
    const noteKey = `${day}-${month}-${year}`;
    const updatedNotes = { ...notes };
    delete updatedNotes[noteKey];
    setNotes(updatedNotes);
  }
};

  const handleSaveText = () => {
    setSelectedDay({ day: null, month: null, year: null });
  };

  return (
    <IonContent style={{ 
      '--background': 'no-repeat center/cover url(public/Fondo.jpg)' 
    }}>
      {!showConfig && selectedDay.day === null ? (
         <IonButton onClick={navigatemenu}>Menu</IonButton>
      ) : null}
      {!showConfig ? (
        selectedDay.day === null ? (
          <Calendar onDayClick={handleDayClick} notes={notes} />
        ) : (
          <div>
            <IonButton onClick={handleBackClick}>Volver al calendario</IonButton>
            <IonButton onClick={handleClearAll}>Eliminar todo</IonButton>
            <IonTextarea value={notes[`${selectedDay.day}-${selectedDay.month}-${selectedDay.year}`] || ''} onIonChange={handleNoteChange} />
            <IonButton onClick={handleSaveText}>Guardar texto</IonButton>
          </div>
        )
      ) : (
        <ConfigUsuario />
      )}
    </IonContent>
  );
};

export default BlocDeNotas;
