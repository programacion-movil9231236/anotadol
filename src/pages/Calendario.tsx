import { IonButton, IonContent } from '@ionic/react';
import React, { useState } from 'react';
import './Calendario.css';

// Propiedades del componente "Calendar"
interface CalendarProps {
  onDayClick: (day: number, month: number, year: number) => void; // Función para manejar el clic en un día del calendario
  notes: Record<string, string>; // Notas asociadas a los días del calendario
}

const Calendar: React.FC<CalendarProps> = ({ onDayClick, notes }) => {
  const [currentMonth, setCurrentMonth] = useState(new Date(2024, 0)); // Fecha del mes actual (inicialmente, enero de 2024)

  const daysInMonth = new Date(currentMonth.getFullYear(), currentMonth.getMonth() + 1, 0).getDate(); // Número de días en el mes actual
  const days = Array.from({ length: daysInMonth }, (_, i) => i + 1); // Array de números del 1 al número de días en el mes
  const weekDays = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']; // Nombres de los días de la semana

  const firstDayOfMonth = new Date(currentMonth.getFullYear(), currentMonth.getMonth(), 1).getDay(); // Día de la semana en que comienza el mes

  const handlePrevMonth = () => {
    setCurrentMonth(new Date(currentMonth.getFullYear(), currentMonth.getMonth() - 1)); // Función para retroceder al mes anterior
  };

  const handleNextMonth = () => {
    setCurrentMonth(new Date(currentMonth.getFullYear(), currentMonth.getMonth() + 1)); // Función para avanzar al mes siguiente
  };

  // Calcular el número de días en blanco al final del calendario
  const blankDaysAtEnd = (7 - ((daysInMonth + firstDayOfMonth) % 7)) % 7;

  return (
    <IonContent style={{ 
      '--background': 'no-repeat center/cover url(/fondo.jpg)' 
    }} className='fondo'>
      {/* Contenedor personalizado para la parte superior del calendario */}
      <div className="calendar-header">
        <IonButton onClick={handlePrevMonth}>Mes anterior</IonButton> {/* Botón para retroceder al mes anterior */}
        <div>{currentMonth.toLocaleDateString('es-ES', { month: 'long', year: 'numeric' })}</div> {/* Mostrar el mes y año actual en formato largo (español) */}
        <IonButton onClick={handleNextMonth}>Mes siguiente</IonButton> {/* Botón para avanzar al mes siguiente */}
      </div>

      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        {weekDays.map((day, index) => (
          <div key={day} style={{ width: '14%', textAlign: 'center' }}>{day}</div> // Renderizar los nombres de los días de la semana
        ))}
        {Array.from({ length: firstDayOfMonth }).map((_, index) => (
          <div key={index} style={{ width: '14%', textAlign: 'center' }}></div> // Renderizar días en blanco al comienzo del mes
        ))}
        {days.map((day) => (
          <div key={day} style={{ width: '14%', textAlign: 'center' }} onClick={() => onDayClick(day, currentMonth.getMonth() + 1, currentMonth.getFullYear())}>
            {day}
            {notes[`${day}-${currentMonth.getMonth() + 1}-${currentMonth.getFullYear()}`] && '...'} {/* Mostrar "..." si hay notas para el día */}
          </div>
        ))}
        {Array.from({ length: blankDaysAtEnd }).map((_, index) => (
          <div key={index + daysInMonth} style={{ width: '14%', textAlign: 'center' }}></div> // Renderizar días en blanco al final del mes
        ))}
      </div>
    </IonContent>
  );
};

export default Calendar;
