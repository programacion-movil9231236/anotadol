// src/pages/DeleteNote.tsx

import {
  IonButton,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import Axios from 'axios';
import React, { useEffect } from 'react';
import { RouteComponentProps } from 'react-router';

const DeleteNote: React.FC<RouteComponentProps<{ id: string }>> = (props) => {
  const noteId = props.match.params.id;

  useEffect(() => {
    // Realiza una solicitud para eliminar la nota
    Axios.delete(`http://localhost:3000/api/notes/${noteId}`)
      .then(() => {
        // Redirige de vuelta a la vista de notas después de la eliminación
        props.history.push('/notes');
      })
      .catch((error) => {
        console.error('Error al eliminar la nota:', error);
      });
  }, [noteId]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Eliminar Nota</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <p>La nota ha sido eliminada con éxito.</p>
        <IonButton routerLink="/notes">Volver a Notas</IonButton>
      </IonContent>
    </IonPage>
  );
};

export default DeleteNote;
