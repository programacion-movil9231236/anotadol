// Importa los componentes necesarios de Ionic y React
import { IonButton, IonCol, IonContent, IonGrid, IonRow } from '@ionic/react';
import React from 'react';
import { useHistory } from 'react-router-dom';
import '../Style.css'; // Importa un archivo de estilos (Style.css)

// Definición del componente Menu
const Menu: React.FC = () => {
  const history = useHistory(); // Instancia el hook useHistory para la navegación

  // Funciones para manejar la navegación a diferentes rutas
  const navigatebloc = () => {
    history.push('/blocdenotas'); // Redirige a la ruta '/blocdenotas'
  };

  const navigateconfigu = () => {
    history.push('/configusuario'); // Redirige a la ruta '/configusuario'
  };

  const navigatenotas = () => {
    history.push('/Notas'); // Redirige a la ruta '/Notas'
  };

  const navigaterutinas = () => {
    history.push('/rutinas'); // Redirige a la ruta '/rutinas'
  };

  const navigategrupo = () => {
    history.push('/grupo'); // Redirige a la ruta '/grupo'
  };

  const navigatehome = () => {
    history.push('/home'); // Redirige a la ruta '/home'
  };

  // Renderiza el contenido del componente
  return (
    <IonContent style={{ 
      '--background': 'no-repeat center/cover url(/fondo.jpg)' 
    }} className='fondo'>
      <IonGrid>
        {/* Row 1: Botones para Calendario y Configuración de usuario */}
        <IonRow className="ion-justify-content-center"> {/* Añade esta clase para centrar los elementos */}
          <IonCol size="6">
            <IonButton expand="full" style={{ fontSize: '20px', padding: '15px' }} onClick={navigatebloc}>
              Calendario
            </IonButton>
          </IonCol>
          <IonCol size="6">
            <IonButton expand="full" style={{ fontSize: '20px', padding: '15px' }} onClick={navigateconfigu}>
              Configuracion de usuario
            </IonButton>
          </IonCol>
        </IonRow>

        {/* Row 2: Botones para Notas, Grupo y Rutinas */}
        <IonRow className="ion-justify-content-center"> {/* Añade esta clase para centrar los elementos */}
          <IonCol size="6">
            <IonButton expand="full" style={{ fontSize: '20px', padding: '15px' }} onClick={navigatenotas}>
              Notas
            </IonButton>
          </IonCol>
          <IonCol size="6">
            <IonButton expand="full" style={{ fontSize: '20px', padding: '15px' }} onClick={navigategrupo}>
              Grupo
            </IonButton>
          </IonCol>
          <IonCol size="6">
            <IonButton expand="full" style={{ fontSize: '20px', padding: '15px' }} onClick={navigaterutinas}>
              Rutinas
            </IonButton>
          </IonCol>
        </IonRow>

        {/* Row 3: Botón para Cerrar Sesión */}
        <IonRow className="ion-justify-content-center"> {/* Añade esta clase para centrar los elementos */}
          <IonCol size="6">
            <IonButton expand="full" style={{ fontSize: '20px', padding: '15px' }} onClick={navigatehome}>
              Cerrar Sesión
            </IonButton>
          </IonCol>
        </IonRow>
      </IonGrid>
    </IonContent>
  );
};

// Exporta el componente Menu
export default Menu;
