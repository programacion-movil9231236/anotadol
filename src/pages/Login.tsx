// Importa los componentes necesarios de Ionic y React
import { IonButton, IonContent, IonInput } from '@ionic/react';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

// Función para obtener el ID del usuario desde el almacenamiento local
const getUserId = () => {
  return localStorage.getItem('userId');
};

// Definición del componente Login
const Login: React.FC = () => {
  // Estados para el nombre de usuario y la contraseña
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  
  const history = useHistory(); // Instancia el hook useHistory para la navegación

  // Función para manejar el inicio de sesión
  const handleLogin = async () => {
    try {
      // Realiza una solicitud POST a la API de login con nombre de usuario y contraseña
      const response = await fetch('http://localhost:3000/api/login', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password }),
      });

      if (response.status === 200) {
        // Si la respuesta es exitosa, obtén el ID de usuario desde la respuesta
        const data = await response.json();
        const userId = data.userId;
      
        // Guarda el ID del usuario en el almacenamiento local
        localStorage.setItem('userId', userId);
      
        // Redirige a la página de menú después del inicio de sesión exitoso
        history.push('/menu');
      } else {
        // Maneja el error si el inicio de sesión no fue exitoso
        const data = await response.json();
        console.error(data.error);
      }
    } catch (error) {
      // Maneja cualquier error de red o en el código
      console.error('Error al iniciar sesión:', error);
    }
  };

  // Función para navegar a la página de inicio (home)
  const navigatehome = () => {
    history.push('/home');
  };

  // Renderiza el contenido del componente
  return (
    <IonContent style={{ 
      '--background': 'no-repeat center/cover url(/fondo.jpg)' 
    }} className='fondo'>
      <h1 style={{ textAlign: 'center', fontSize: '24px', margin: '20px' }}>Login</h1>
      
      {/* Input para el nombre de usuario */}
      <IonInput
        placeholder="Nombre de usuario"
        value={username}
        onIonChange={(e) => setUsername(e.detail.value!)}
      />
      
      {/* Input para la contraseña */}
      <IonInput
        placeholder="Contraseña"
        type="password"
        value={password}
        onIonChange={(e) => setPassword(e.detail.value!)}
      />
      
      {/* Botón para iniciar sesión */}
      <IonButton onClick={handleLogin}>Iniciar Sesión</IonButton>
      
      {/* Botón para regresar a la página de inicio */}
      <IonButton onClick={navigatehome}>Regresar</IonButton>
    </IonContent>
  );
};

// Exporta el componente Login
export default Login;
